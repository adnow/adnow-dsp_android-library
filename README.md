# AdNow - Android Library 1.0

__While this library is compatible with Eclipse/ADT, we strongly advise you to use Android Studio for the simplicity of its setup.__

## INSTALLATION/REQUIREMENTS

### Android Studio

Copy the provided `adnow-library.aar` file into the `libs/` directory of your app.
In your app `build.gradle` file, add the following repository (if needed) and dependency instruction as follow:

```
#!groovy

    repositories {
        # ...
        flatDir {
            dirs 'libs'
        }
        # ...
    }
    dependencies {
        # ...
        compile(name:'adnow-library', ext:'aar')
        # ...
    }

```

Android Studio users can skip to the SETUP section of this document.

### Eclipse/ADT

Import the Eclipse Library Project `adnow-library-eclipse/` into your workspace (`Import... > Android > Existing Android Code Into Workspace`).

In this newly imported project, ensure that the checkbox `Is Library` is checked in the `Properties > Android` screen.

On your project's `Properties > Android` screen, in the Library section, add a reference to the imported adnow-library project (which **MUST** still be opened in Eclipse).

#### Additionnal requirements (Eclipse/ADT only)
- [android-support-v7-appcompat with resources](https://developer.android.com/tools/support-library/setup.html)
- [google-play-services_lib](http://developer.android.com/google/play-services/setup.html)
- [volley framework](http://developer.android.com/training/volley/index.html)
- [google-gson 2.3.+](https://code.google.com/p/google-gson/)


## SETUP

Initialize the library by calling `AdNowSDK.init()` in the overridden `Application.onCreate` method of your project.

If you don't have an `Application` class, make sure to create one like below:

```
#!java

    package io.adnow.myproject;

    import android.app.Application;
    import io.adnow.dsp.sdk.lib.AdNowSDK;

    public class MyApplication extends Application {
        @Override
        public void onCreate() {
            super.onCreate();

            AdNowSDK.init(this); // "this" refers to an instance of android.app.Application

            // the rest of your code
            // ...
        }
    }

```

Don't forget to declare its usage in the `AndroidManifest.xml` file on the `<application>` element:

```
#!xml

    <application [...] android:name="io.adnow.myproject.MyApplication">
        <!-- List of your project's activities, etc, same as any other project -->
    </application>

```

Some permissions and activities are used and started by the AdNow Library, Android Studio users don't need to do anything about this because they are declared in the adnow-library.aar file.
However, Eclipse/ADT **MUST** ensure that the following are set in the `AndroidManifest.xml` file:

```
#!xml

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

    <application [...] android:name="io.adnow.myproject.MyApplication">
        <!-- List of your project's activities, etc, same as any other project -->

        <!-- For AdNow Library -->
        <activity
            android:name="io.adnow.dsp.sdk.lib.activity.AdNowWebViewActivity"
            android:configChanges="orientation|keyboardHidden|screenSize">
        </activity>
        <activity
            android:name="io.adnow.dsp.sdk.lib.activity.AdNowInterstitialActivity"
            android:configChanges="orientation|keyboardHidden|screenSize">
        </activity>
    </application>

```


## USAGE

This version of the AdNow Library provides 2 visual containers:

- banners
- interstitials

Each of those container in your application is an advertising space and **MUST** have it's own unique identifier, called a `placement identifier`, and provided by AdNow.


### Banner

A banner is a `View` container that will feature advertising contents through a visual image ; when the image is clicked by the user, a `WebView` shows up and displays a website, called landing page.
This `View` container behaves like any other Android view and can be place anywhere in your layouts.

First, you must set up the container in your `Layout`:

```
#!xml

    <io.adnow.dsp.sdk.lib.view.AdNowBannerView
        xmlns:adnow="http://schemas.android.com/apk/res-auto"
        android:id="@+id/adnow_banner"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        adnow:placementId="yourUniquePlacementId"
        adnow:keywords="list,of,keywords"
        adnow:useGeolocation="true/false"/>

```

#### Attributes

- `placementId`: _String_ **required**
    - the unique identifier of this advertising space
- `keywords`: _String_
    - a list of words to qualify this advertising space
- `useGeolocation`: _boolean_
    - if set to `true` the AdNow Library will provide the user's last known geolocation (see PROVIDING GEOLOCATION section) when requesting the AdNow advertising servers


### Java manipulation
After the `AdNowBannerView` is inflated in your layout, you **MUST** start its lifecycle by calling the `AdNowAdView.fetchAd()` method. For example:

```
#!java

    AdNowAdView simpleBanner = (AdNowAdView) findViewById(R.id.adnow_banner); // find the view
    simpleBanner.fetchAd(); // start lifecycle

```

Several methods are available to manipulate your banner:

- `public void fetchAd()`:
    - starts the view's lifecycle
- `public void setKeywords(String keywords)`:
    - sets the keywords for the view (`null` to unset previously the set ones)
- `public void setAdViewListener(AdNowAdViewListener listener)`:
    - set the view listener where the listener implements `AdNowAdViewListener` interface ;
    - `AdNowAdViewListener` methods:
        - `void onAdLoaded(AdNowAdView view)`:
            - called when an ad is loaded into the view
        - `void onAdError(AdNowAdView view, AdNowErrorType errorType)`:
            - called when an error occurred while loading the ad
        - `void onAdClicked(AdNowAdView view)`:
            - called when the ad is clicked by the user

#### Advanced example

```
#!java

    AdNowAdView simpleBanner = (AdNowAdView) findViewById(R.id.adnow_banner); // find the view
    simpleBanner.setKeywords("croissant,chocolate");
    simpleBanner.setAdViewListener(new AdNowAdViewListener() {
        @Override
        public void onAdLoaded(final AdNowAdView view) {
            // a new Ad is currently displayed to the user, try to make him click
            Toast.makeText(getApplicationContext(), "Hey, you should click on this Ad !", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onAdError(final AdNowAdView view, final AdNowErrorType errorType) {
            // something was KO when loading a new Ad (don't do anything)
        }

        @Override
        public void onAdClicked(final AdNowAdView view) {
            Toast.makeText(getApplicationContext(), "Thank you for having clicked this Ad !", Toast.LENGTH_SHORT).show();
        }
    });
    simpleBanner.fetchAd(); // start lifecycle

```
### Interstitial

An interstitial is an `Activity` that will feature advertising contents through visual fullscreen image ; when the image is clicked by the user, a `WebView` shows up and displays a website, called landing page.

You can start an interstitial whenever and wherever you want.

### Java instantiation and manipulation

```
#!java

    AdNowInterstitial interstitial = new AdNowInterstitial(getApplicationContext(), "yourUniquePlacementId", true); // instantiate the interstitial with geolocation
    interstitial.fetchAd(); // start lifecycle

```

Several methods are available to manipulate your interstitial:

- `public AdNowInterstitial(Context context, String placementId, boolean useGeolocation)`: (constructor)
    - builds a new interstitial instance for the unique placement
- `public void fetchAd()`:
    - starts the interstitial's lifecycle
- `public void setKeywords(String keywords)`:
    - sets the keywords for the view (`null` to unset previously the set ones)
- `public void setAdViewListener(AdNowInterstitialListener listener)`:
    - set the interstitial listener where the listener implements `AdNowInterstitialListener` interface ;
    - `AdNowInterstitialListener` methods:
        - `void onInterstitialStarted(AdNowInterstitial interstitial)`:
            - called when an ad is loaded into the interstitial container (activity)
        - `void onInterstitialFinished(AdNowInterstitial interstitial)`:
            - called when the interstitial is closed
        - `void onInterstitialError(AdNowInterstitial interstitial, AdNowErrorType type)`:
            - called when an error occurred while loading the ad
        - `void onInterstitialClicked(AdNowInterstitial interstitial)`:
            - called when the ad is clicked by the user


## PROVIDING GEOLOCATION

AdNow Library can use the user's geolocation to provide better ad targeting.

While development to retrieve user's geolocation is left to you, you can provide the `Location` to the AdNow Library using the `AdNowSDK.setLocationData(Location)` method. For example:

```
#!java

    // you project own location listener
    public void onLocationChanged(Location location) {
        AdNowSDK.getInstance().setLocationData(location);
        // the rest of your code
        // ...
    }

```
